package com.imaginnovate.keycloak.controller;

import javax.annotation.security.RolesAllowed;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/key")
public class KeycloakController {
	
	@GetMapping("/adminCheck")
	@RolesAllowed("admin")
	String getDummy() {
		return "admin";
	}
	
	@GetMapping("/userCheck")
	@RolesAllowed("user")
	String getDummy1() {
		return "user";
	}

}
